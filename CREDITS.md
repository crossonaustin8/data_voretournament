# Coding

## Main Programmer

MirceaKitsune
- https://gitlab.com/voretournament/data_voretournament/tree/master/mod
- https://furaffinity.net/user/mircea

# Models

## Anthropomorphic Wolf

UntiedVerbeger, MirceaKitsune
- https://blendswap.com/blends/view/79735
- https://furaffinity.net/user/untiedverbeger

# Textures

## 2048x2048 Tiling Beast Fur Texture

bart
- https://opengameart.org/content/2048x2048-tiling-beast-fur-texture
- https://opengameart.org/users/bart

## 369_tile_Goo

Patrick Hoesly
- https://everystockphoto.com/photo.php?imageId=8460179
- https://everystockphoto.com/photographer.php?photographer_id=30411

# Vector

## Wolf

Johnny Automatic
- https://openclipart.org/detail/6111/wolf-by-johnny_automatic-6111
- https://openclipart.org/user-detail/johnny_automatic

## Buddy

Sheikh Tuhin
- https://openclipart.org/detail/19935/buddy-by-sheikh_tuhin-19935
- https://openclipart.org/user-detail/sheikh_tuhin

# Sounds

## Swallowing Gulp

Gregory Weir
- https://pdsounds.org/sounds/swallowing_gulp
- https://pdsounds.org/audio/by/artist/gregory_weir

## Burp

Ezwa
- https://pdsounds.org/sounds/burp
- https://pdsounds.org/audio/by/artist/ezwa

## Man Coughing

Stilgar
- https://pdsounds.org/sounds/man_coughing
- https://pdsounds.org/users/stilgar

## Tummy Grumble

Esther
- https://pdsounds.org/sounds/tummy_grumble
- https://pdsounds.org/audio/by/artist/esther

## Sinister Laugh

WeaponGuy
- https://opengameart.org/content/sinister-laugh
- https://opengameart.org/user/301

## So that's coming along...

bart
- https://opengameart.org/content/so-thats-coming-along
- https://opengameart.org/user/1

## 37 hits/punches

Independent.nu
- https://opengameart.org/content/37-hitspunches
- https://johannespinter.com/inu/ljudbank.html

## Chant

christislord
- https://opengameart.org/content/chant
- https://opengameart.org/users/christislord

# Music

## Telosbgm2

Tozan
- https://opengameart.org/content/telosbgm2
- https://opengameart.org/users/tozan

# Fonts

## Peace Sans

Sergey Ryadovoy, Ivan Gladkikh (Jovanny Lemonad)
- https://dafont.com/peace-sans.font
- https://dafont.com/profile.php?user=194118

# Special Supporters

## Qoostewin

- https://patreon.com/user/creators?u=486197

## Smokey

- https://patreon.com/user/creators?u=2588581

## Digdug

- https://patreon.com/user/creators?u=220075

## Michael Ober

- https://patreon.com/user/creators?u=3602919
